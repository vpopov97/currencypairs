module.exports = {
    devServer: {
        // for options see https://github.com/chimurai/http-proxy-middleware#proxycontext-config
        proxy: {
            '^/api/v1': {
                target: 'http://localhost:3000/',
                ws: true, // enable proxy websockets
                changeOrigin: true,
                onProxyReq: function (request) {
                    request.setHeader('origin', 'http://localhost:3000/')
                }
            }
        }
    },
    configureWebpack: (config) => {
        config.devtool = 'eval-source-map'
    }
}
