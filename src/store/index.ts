import Vue from 'vue'
import Vuex from 'vuex'
import { createStore, Module } from 'vuex-smart-module'
import { CurrencyPairs } from '@/store/CurrencyPairs'

Vue.use(Vuex)

const root = new Module({
  modules: {
    CurrencyPairs
  }
})

// The 1st argument is root module.
// Vuex store options should be passed to the 2nd argument.
export const store = createStore(
    // Root module
    root,

    {
      strict: true
    }
)
