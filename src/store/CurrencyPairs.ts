import { Getters, Mutations, Actions, Module } from 'vuex-smart-module'
import axios from 'axios'
import { marketUrl, marketCurrency, currencyList, marketCurrencyLabel, updateCurrencyPayload } from './CurrencyPairsInterface'


class CurrencyPairsState {
    currencyLoading = false;
    marketCurrency: marketCurrency = {
        first: {
            EUR: 1,
            USD: 1,
            RUB: 1
        },
        firstPoll: {
            EUR: 1,
            USD: 1,
            RUB: 1
        },
        second: {
            EUR: 1,
            USD: 1,
            RUB: 1
        },
        secondPoll: {
            EUR: 1,
            USD: 1,
            RUB: 1
        },
        third: {
            EUR: 1,
            USD: 1,
            RUB: 1
        },
        thirdPoll: {
            EUR: 1,
            USD: 1,
            RUB: 1
        }
    }
}
class CurrencyPairsGetters extends Getters<CurrencyPairsState> {
}

function instanceOfCurrencyPayload(object: any): object is updateCurrencyPayload {
    return 'marketCurrencyLabel' in object;
}

// Мутации
class CurrencyPairsMutations extends Mutations<CurrencyPairsState> {
    UPDATE_CURRENCY_LOADING (payload: boolean): void {
        this.state.currencyLoading = payload
    }
    UPDATE_MARKET_CURRENCY (payload: any): void {
        if (instanceOfCurrencyPayload(payload)) {
            this.state.marketCurrency[payload.marketCurrencyLabel] = payload.content
        }
    }
}

class CurrencyPairsActions extends Actions<
    CurrencyPairsState,
    CurrencyPairsGetters,
    CurrencyPairsMutations,
    CurrencyPairsActions
    > {
    updateMarket (marketUrl: marketUrl) {
        const commit = this.commit
        const dispatch = this.dispatch
        commit('UPDATE_CURRENCY_LOADING', true)
        axios({
            method: 'get',
            url: process.env.VUE_APP_API_URL + marketUrl
        })
            .then(function (response) {
                const label: string = marketUrl
                    .replace(/[/]([a-z])/g, function (g) { return g[1].toUpperCase(); })
                const payload: any = {
                    marketCurrencyLabel: label,
                    content: response.data.rates
                }
                commit('UPDATE_MARKET_CURRENCY', payload)
            })
            .catch(function (e) {
                console.log('ошибка', e)
            })
            .finally(() => {
                setTimeout(() => {
                    commit('UPDATE_CURRENCY_LOADING', false)
                    dispatch('updateMarket', marketUrl)
                }, 1000)

            })
    }
}

export const CurrencyPairs = new Module({
    state: CurrencyPairsState,
    getters: CurrencyPairsGetters,
    mutations: CurrencyPairsMutations,
    actions: CurrencyPairsActions
})
