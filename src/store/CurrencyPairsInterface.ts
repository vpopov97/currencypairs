type marketUrl = 'first'|'first/poll'|'second'|'second/poll'|'third'|'third/poll'
type marketCurrencyLabel = 'first'|'firstPoll'|'second'|'secondPoll'|'third'|'thirdPoll'
type currencyList = {
    EUR: number,
    USD: number,
    RUB: number
}
interface marketCurrency {
    first: currencyList
    firstPoll: currencyList
    second: currencyList
    secondPoll: currencyList
    third: currencyList
    thirdPoll: currencyList

}
interface updateCurrencyPayload {
    marketCurrencyLabel: marketCurrencyLabel
    content: currencyList
}
export {
    marketUrl, marketCurrency, currencyList, marketCurrencyLabel, updateCurrencyPayload
}