import {createDecorator, VueDecorator} from 'vue-class-component'
import { mapState } from "vuex";
export function MapState (namespace: string, states: any): VueDecorator {
    return createDecorator(options => {
        if (!options.computed) {
            options.computed = {}
        }
        Object.assign(options.computed, mapState(namespace, states))
    })
}
